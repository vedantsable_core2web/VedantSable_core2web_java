

class ques9_C2W{

	public static void main(String[] args){
        
        double acTemperature = 22.5; 
        int standardRoomTemperature = 25; 

        System.out.println("Temperature of the Air Conditioner: " + acTemperature + " degrees");
        System.out.println("Standard Room Temperature: " + standardRoomTemperature + " degrees");

	}
}
