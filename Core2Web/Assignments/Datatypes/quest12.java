

class quest12_C2W{

    public static void main(String[] args) {
        
	double liters = 5.7;
        double densityOfWater = 1000;
        double weightInGrams = liters * densityOfWater * 1000;

        System.out.println("Quantity of liters: " + liters +" L");
        System.out.println("Weight in grams: " + weightInGrams +" g");
	
    	}
}
