

class quest15_C2W{

	public static void main(String[] args){

	String brand = "Toyota";
        int year = 2022; 
        double price = 25000.50; 
        boolean isAutomatic = true;
        char fuelType = 'P';

        
        System.out.println("Brand: " + brand);
        System.out.println("Year: " + year);
        System.out.println("Price: $" + price);
        System.out.println("Automatic: " + isAutomatic);
        System.out.println("Fuel Type: " + fuelType);
	
	}
}
