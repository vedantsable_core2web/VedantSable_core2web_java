import java.io.*;

class prog6_C2W{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter no. of rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i<=row ; i++){
			
			int num = row;

			for(int j = 1; j<=i ; j++ ){
				
			if(i%2==1){	
			System.out.print((char)(num+96) + "\t");
			}else{
			System.out.print(num + "\t");
			}
			num--;
			}
			System.out.println();
		}			

	}

}
