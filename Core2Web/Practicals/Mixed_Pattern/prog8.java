import java.io.*;

class prog8_C2W{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter no. of rows : ");
		int row = Integer.parseInt(br.readLine());
		
		int ch  = (row*(row+1))/2;
		
		for(int i = 1 ; i<=row ; i++){
			
			for(int j = 1;j<=row-i+1;j++){

				System.out.print((char)(ch+64) + "\t");
				ch--;
			}
			System.out.println();
		}			

	}

}
