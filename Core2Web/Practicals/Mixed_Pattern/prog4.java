import java.io.*;

class prog4_C2W{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter no. of rows : ");
		int row = Integer.parseInt(br.readLine());
		
		int num = row;

		for(int i = 1; i<=row ; i++){

			for(int j = 1; j<=i ; j++ ){
			
			System.out.print(num*j + "\t");
			
			}
			num--;
			System.out.println();
		}			

	}

}
