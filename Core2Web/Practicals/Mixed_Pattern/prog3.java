import java.io.*;

class prog3_C2W{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter no. of rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i=1; i<=row ; i++){
			
			int ch = row;

			for(int j=1; j<=row ; j++){
				
				if(i%2==1){
				System.out.print((char)(ch+64) + "\t");
				ch--;
				}else{
				System.out.print(j + "\t");
				}
			}
			System.out.println();
		}

	}

}

