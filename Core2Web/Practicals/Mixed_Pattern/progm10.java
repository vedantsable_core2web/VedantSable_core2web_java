import java.io.*;

class progm10_C2W{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter no. : ");
		long num = Integer.parseInt(br.readLine());

		long temp = num;
		long rev = 0;

		while(temp>0){

			long rem = temp%10;
			
			rev = (rev*10)+rem;

			temp/=10;

		}

		long temp1 = rev;

		while(temp1>0){

			long rem1 = temp1%10;

			if(rem1%2==1){

				System.out.print(rem1*rem1 + ",");

			}
			
			temp1/=10;
		}
	}

}
