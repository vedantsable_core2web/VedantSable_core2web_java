import java.io.*;

class prog2_C2W{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter no. of rows : ");
		int row = Integer.parseInt(br.readLine());
		
		int num = row;
		int ch = row;

		for(int i=1; i<=row ; i++){
			
			for(int j=1; j<=row ; j++){	

				System.out.print((char)(ch+64) + "" + num-- + "\t");

			}
			num = row+i;
			System.out.println();
		}

	}

}

