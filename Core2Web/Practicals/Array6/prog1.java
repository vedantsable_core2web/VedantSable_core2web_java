import java.util.*;

class prog1_C2W{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Array size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter array elements :");
		for(int i=0;i<size;i++){
			arr[i] = sc.nextInt();
		}

		int flag=1;
		for(int i=0;i<size-1;i++){

			if(arr[i]>arr[i+1]){
				flag=0;
				break;
			}
		}

		if(flag==0){
			System.out.println("In descending order");
		}
		else{
			flag=1;
			System.out.println("Not in descending order");
		}
	}
}



