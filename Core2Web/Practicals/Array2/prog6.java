import java.util.*;

class prog6_C2W{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter Size : ");	

		int Size = sc.nextInt();

		int arr[] = new int[Size];
	
		System.out.println("Enter array element : ");

		for (int i = 0 ; i<arr.length ; i++){

			arr[i] = sc.nextInt();

		}
	
		int product = 1;
		
		//System.out.println("Array element are : ");
		for(int i = 0 ; i<arr.length ; i++ ){
			
			if(i%2==1){
			product*=arr[i];
			}
		}
		System.out.println("Product of odd indexed elements : " + product);
	}

}
