import java.util.*;

class prog1_C2W{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter Size : ");	

		int Size = sc.nextInt();

		int arr[] = new int[Size];
	
		System.out.println("Enter array element : ");

		for (int i = 0 ; i<arr.length ; i++){

			arr[i] = sc.nextInt();

		}
		
		System.out.println("Even no. are : ");
		int count = 0;
		for(int i = 0 ; i<arr.length ; i++ ){
			
			if(arr[i]%2==0){
			System.out.println(arr[i]);
			count++;
			}

		}
		System.out.println("Count of Even no. is " + count);
	}

}
