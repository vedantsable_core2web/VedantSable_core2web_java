import java.util.*;

class prog1_C2W{
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number : ");
		int num = sc.nextInt();

		if(num<=0){
			System.out.println("Negative number cannot be perfect Enter positive number");
			return;
		}
		else{
			int sum=0;
			for(int i=1;i<=num/2;i++){
				if(num%i==0){
					sum+=i;
				}
			}
			if(sum==num){
				System.out.println(num+" is a perfect number");
			}
			else{
				System.out.println(num+" is not a perfect number");
			}
		}
	}
}



