

class ques4_C2W{

	public static void main(String[] args){

		char ch = 't';

		if( ch >= 'a' || ch <= 'b'){

			System.out.println( ch + " is lowercase letter");

		}else if( ch >= 'A' || ch <= 'B'){

			System.out.println( ch + " is UPPERCASE letter");

		}else{
			System.out.println("Not a letter");
		}

	}

}
