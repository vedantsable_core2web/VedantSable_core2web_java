import java.util.*;

class prog9_C2W{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter Size : ");	

		int Size = sc.nextInt();

		int arr[] = new int[Size];
	
		System.out.println("Enter array element : ");

		for (int i = 0 ; i<arr.length ; i++){

			arr[i] = sc.nextInt();
	
		}
		
		System.out.println("Prime element are : ");
	
		for(int i = 0 ; i<arr.length ; i++ ){
	
			int count = 0;
				
			for(int j = 1 ; j<=arr[i] ; j++){

				if(arr[i]%j==0){
				count++;
				}
			}

			if(count==2){
				System.out.println(arr[i] + "\t");
			}

		}
		
	}
}
