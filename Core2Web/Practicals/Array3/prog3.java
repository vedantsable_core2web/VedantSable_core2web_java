import java.util.*;

class prog3_C2W{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter Size : ");	

		int Size = sc.nextInt();

		int arr[] = new int[Size];
	
		System.out.println("Enter array element : ");

		for (int i = 0 ; i<arr.length ; i++){

			arr[i] = sc.nextInt();
		}
		
		System.out.println("Specific number : ");
		int num = sc.nextInt();
		
		int count = 0;

		System.out.println("Array element are : ");
		for(int i = 0 ; i<arr.length ; i++ ){
			
			if(arr[i]==num){
			count++;
			}
		}
		if(count>0){
		System.out.println("num " + num + " Occurred " + count +" times in array");
		}else{
		System.out.println("num " + num + " not found in array");
		}
	}

}
