import java.util.*;

class prog5_C2W{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter Size : ");	

		int Size = sc.nextInt();

		int arr[] = new int[Size];
	
		System.out.println("Enter array element : ");

		for(int i = 0 ; i<arr.length ; i++){

			arr[i] = sc.nextInt();
	
		}
		
		System.out.println("Reversed array is : ");

		for(int i = 0 ; i<Size/2 ; i++){

			int temp = arr[i];
			arr[i] = arr[Size - i - 1];
			arr[Size - i - 1] = temp;
			}

		for(int i  = 0 ; i<Size ; i++){

			System.out.println(arr[i]);
		}
		
	}
}
