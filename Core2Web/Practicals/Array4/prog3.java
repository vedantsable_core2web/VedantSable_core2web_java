import java.util.*;

class prog3_C2W{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter Size : ");	

		int Size = sc.nextInt();

		int arr[] = new int[Size];
	
		System.out.println("Enter array element : ");

		for (int i = 0 ; i<arr.length ; i++){

			arr[i] = sc.nextInt();
	
		}
		
		int max = arr[0];
	
		for(int i = 0 ; i<arr.length ; i++ ){
	
			if(arr[i]>max){

				max = arr[i];

			}

		}

		int max2 = arr[0];

		for(int i = 0 ; i<arr.length ; i++){

			if(arr[i]>=max2 && arr[i]<max){

				max2 = arr[i];

			}

		}

		System.out.println("The Second largest element is : " + max2);
		
	}
}
