import java.util.*;

class prog9_C2W{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter Size : ");	

		int Size = sc.nextInt();

		char carr[] = new char[Size];
	
		System.out.println("Enter array element : ");

		for (int i = 0 ; i<carr.length ; i++){

			carr[i] = sc.next().charAt(0);

		}
		
		System.out.println("Array element are : ");
	
		for(int i = 0 ; i<carr.length ; i++ ){
			
			if(carr[i]<'a' || carr[i]>'z'){

				carr[i] = '#';

			}
			
			System.out.println(carr[i]);
		}
		
	}

}
