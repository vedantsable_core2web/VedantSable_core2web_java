import java.util.*;

class prog8_C2W{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter Size : ");	

		int Size = sc.nextInt();

		char carr[] = new char[Size];
	
		System.out.println("Enter array element : ");

		for (int i = 0 ; i<carr.length ; i++){

			carr[i] = sc.next().charAt(0);

		}
		
		int count = 0;

		System.out.println("Enter character to check :");
		char ch = sc.next().charAt(0);

		System.out.println("Array element are : ");
	
		for(int i = 0 ; i<carr.length ; i++ ){
			
			if(carr[i]==ch){

				count++;
			}
		}

		System.out.println(ch + " occurs " + count + " times in the given array");
		
	}

}
