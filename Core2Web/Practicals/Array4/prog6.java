import java.util.*;

class prog6_C2W{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter Size : ");	

		int Size = sc.nextInt();

		char carr[] = new char[Size];
	
		System.out.println("Enter array element : ");

		for (int i = 0 ; i<carr.length ; i++){

			carr[i] = sc.next().charAt(0);

		}

		int count1 = 0;

		int count2 = 0;

		System.out.println("Array element are : ");
	
		for(int i = 0 ; i<carr.length ; i++){
			
			
			if(carr[i]=='a'||carr[i]=='e'||carr[i]=='i'||carr[i]=='o'||carr[i]=='u'||carr[i]=='A'||carr[i]=='E'||carr[i]=='I'||carr[i]=='O'||carr[i]=='U'){
			count1++;
			}else{
			count2++;
			}
		}

		System.out.println("Count of vowels : " + count1);

		System.out.println("Count of Consonants : " + count2);
		
	}

}
