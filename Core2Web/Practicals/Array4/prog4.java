import java.util.*;

class prog4_C2W{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter Size : ");	

		int Size = sc.nextInt();

		int arr[] = new int[Size];
	
		System.out.println("Enter array element : ");

		for (int i = 0 ; i<arr.length ; i++){

			arr[i] = sc.nextInt();
	
		}

		System.out.println("Enter number to check : ");
		int num = sc.nextInt();
		
		int count = 0;
	
		for(int i = 0 ; i<arr.length ; i++ ){
	
			if(arr[i]==num){

				count++;

			}

		}
		
		if(count>2){
		System.out.println(num + " Occurred more than 2 times");
		}else if(count == 2){
		System.out.println(num + " Occurred exactly 2 times");
		}else{
		System.out.println(num + " Occurred less than 2 times");
		}

	}

}
