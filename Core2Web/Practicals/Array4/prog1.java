import java.util.*;

class prog1_C2W{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter Size : ");	

		int Size = sc.nextInt();

		int arr[] = new int[Size];
	
		System.out.println("Enter array element : ");
		
		int sum = 0;

		for (int i = 0 ; i<arr.length ; i++){

			arr[i] = sc.nextInt();
			sum+=arr[i];
		}
		
		System.out.println("Array elements' average is : " + sum);		

		}
		
	}

