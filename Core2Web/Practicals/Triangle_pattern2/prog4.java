import java.io.*;

class prog4_C2W{

        public static void main(String[] args) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter the no. of rows : ");

                int row = Integer.parseInt(br.readLine());

                for(int i = 1; i<=row ; i++){
			int ch1 = row+96;
			int ch2 = row+64;
                        for(int j = 1; j<=i ; j++){
				
				if(i%2==1){
                                System.out.print((char)ch1-- + " ");
				}else{
				System.out.print((char)ch2-- + " ");
                        	}
			}
                        System.out.println();

                }

        }

}
