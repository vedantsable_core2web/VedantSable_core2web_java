import java.io.*;

class prog5_C2W{

        public static void main(String[] args) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter the no. of rows : ");

                int row = Integer.parseInt(br.readLine());
		
		int ch = row+64;

                for(int i = 1; i<=row ; i++){

                        for(int j = 1; j<=i ; j++){

                                System.out.print((char)ch++ + " ");

                        }

                        System.out.println();

                }

        }

}
