import java.io.*;

class prog8_C2W{

        public static void main(String[] args) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter the no. of rows : ");

                int row = Integer.parseInt(br.readLine());

		char ch = 'c';
		
                for(int i = 1; i<=row ; i++){

                        for(int j = 1; j<=i ; j++){
				
				if(j%2==1){
                                System.out.print(j + " ");
				}else{
				System.out.print(ch + " ");
				ch+=2;
				}
                        }

                        System.out.println();

                }

        }

}
