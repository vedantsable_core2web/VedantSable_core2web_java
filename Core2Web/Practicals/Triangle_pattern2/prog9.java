import java.io.*;

class prog9_C2W{

        public static void main(String[] args) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter the no. of rows : ");

                int row = Integer.parseInt(br.readLine());
		
		int ch = 97;

                for(int i = 1; i<=row ; i++){
			
			int num = row+1;

                        for(int j = 1; j<=i ; j++){
					
				if(j%2==1){
                                System.out.print(num + " ");
				}else{
				System.out.print((char)ch++ + " ");
				}
				num++;
                        }

                        System.out.println();

                }

        }

}
