import java.io.*;

class prog2_C2W{

        public static void main(String[] args) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter the no. of rows : ");

                int row = Integer.parseInt(br.readLine());

                for(int i = 1; i<=row ; i++){

                        for(int j = 1; j<=i ; j++){

                     		if(i%2==1){

		     		System.out.print((char)(j+96) + "\t");
                        	
				}else{

				System.out.print("$"+"\t");
				
				}
			}
                        System.out.println();
		}
        }
}
