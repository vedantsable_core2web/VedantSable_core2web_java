import java.io.*;

class prog1_C2W{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter no. of Rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i<=row ; i++){

			for(int space = 1 ; space <= row-i ; space++){

				System.out.print(" " + "\t");

			}

			for(int j = 1; j <= i ; j++ ){

				System.out.print(j + "\t");

			}

			System.out.println();

		}

	}

}

