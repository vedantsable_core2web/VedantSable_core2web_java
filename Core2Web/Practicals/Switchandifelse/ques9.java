

class ques9_C2W{

	public static void main(String[] args){
        
        System.out.println("Enter marks for 5 subjects:");
        int totalMarks = 0;
        boolean hasFailed = false;
     	
	int math = 45;
	if(math < 40){
                hasFailed = true;
            }
	int Physics = 60;
	if(Physics < 40){
                hasFailed = true;
            }
	int science = 55;
	if(science < 40){
                hasFailed = true;
            }
	int hindi = 57;
	if(hindi < 40){
                hasFailed = true;
            }
	int drawing = 80;
	if(drawing < 40){
                hasFailed = true;
            }
	
            totalMarks = math + Physics + science + hindi + drawing;
        
        if(hasFailed){
            System.out.println("You failed the exam");
        }else{
            int averageMarks = totalMarks / 5;
            switch(averageMarks / 10) {
                case 9:
                case 10:
                    System.out.println("First class with distinction");
                    break;
                case 8:
                    System.out.println("First class");
                    break;
                case 7:
                    System.out.println("Second class");
                    break;
                default:
                    System.out.println("Pass");

	    		}
		}
	}
}
