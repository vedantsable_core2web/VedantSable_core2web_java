

class ques8_C2W{

	public static void main(String[] args){

		float percent = 13.00f;

		if(percent >= 75.00){

			System.out.println("Passed : first class with distinction");

		}else if(percent >= 60.00 && percent < 70.00){

			System.out.println("Passed : first class");

		}else if(percent >= 50.00 && percent < 60.00){

			System.out.println("Passed : second class");

		}else if(percent >= 35.00 && percent < 50.00){

			System.out.println("Pass");

		}else if(percent >= 0){

			System.out.println("Failed with distinction");

		}else{

			System.out.println("Enter a valid percent");

		}

	}

}

