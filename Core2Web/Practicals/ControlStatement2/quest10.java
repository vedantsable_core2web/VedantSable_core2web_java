

class ques10_C2W{

	public static void main(String[] args){

		System.out.println("*************Laptop Suggestion System************");

		int budget = 100000;

		if(budget>120000){

			System.out.println("You can buy a Macbook");

		}else if(budget > 80000){

			System.out.println("You can buy a Gaming Laptop");

		}else if(budget > 40000){

			System.out.println("You can buy a Non-Gaming Laptop");

		}else{

			System.out.println("Go for tablets or pads");

		}

	}

}
