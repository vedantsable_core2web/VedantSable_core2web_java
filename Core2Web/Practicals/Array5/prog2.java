import java.util.*;

class prog2_C2W{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter size of array : ");
                int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array elements : ");

		for(int i=0;i<size;i++){
			arr[i] = sc.nextInt();
		}

		int ESum=0;
		int OSum=0;

		for(int i=0;i<size;i++){

			if(arr[i]%2==0){
		
				ESum+=arr[i];
			}

			else{
		
				OSum+=arr[i];
			}
		}

		System.out.println("EvenSum = "+ ESum);
		System.out.println("OddSum = "+ OSum);
	}
}


