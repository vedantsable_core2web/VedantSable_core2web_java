import java.util.*;

class prog1_C2W{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter size of array : ");
                int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Elements : ");
		
		for(int i=0;i<size;i++){
			arr[i] = sc.nextInt();
		}

		int flag=1;

		for(int i=0;i<size-1;i++){

			if(arr[i]<arr[i+1]){
				flag=1;
			}else{
				flag=0;
			}
		}

		if(flag==1){
			System.out.println("The given array in ascending order");
		}
		else{
			System.out.println("The given array not in ascending order");
		}
	}
}

