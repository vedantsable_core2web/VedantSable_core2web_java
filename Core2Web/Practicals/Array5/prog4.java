import java.util.*;

class prog4_C2W{

        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);

                System.out.print("Enter size of array : ");
                int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array elements : ");
		for(int i=0;i<size;i++){
			arr[i] = sc.nextInt();
		}

		int flag=1;
		int Index=0;

		for(int i=0;i<size;i++){

			for(int j=i+1;j<size;j++){

				if(arr[i]==arr[j]){

					flag=0;
					Index=i;
					break;
				}
			}

			if(flag==0){
				break;
			}
		}

		if(flag==1){

			System.out.println("Duplicate element found ");
		
		}
		
		else{

			System.out.println("First duplicate element present at index "+Index);
		
		}
	}
}



