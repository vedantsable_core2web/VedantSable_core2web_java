import java.util.*;

class prog6_C2W{

        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);

                System.out.print("Enter size of array : ");
                int size = sc.nextInt();

                int arr[] = new int[size];

	
		System.out.println("Enter Array elements : ");
        
		for(int i=0;i<size;i++){
                        arr[i] = sc.nextInt();
			
		}
		
		int ind=-1;
		for(int i=0;i<size;i++){
			int num=arr[i];
			int cnt=0;
			for(int J=1;J<=num;J++){

				if(num%J==0){
					cnt++;
				}
			}
			if(cnt==2){
				ind=i;
				break;
			}
		}
		if(ind!=-1){
			System.out.println("First prime number found at index "+ind);
		}
		else{
			System.out.println("No prime number found in the array");
		}
		
	}
}


