import java.util.*;

class prog9_C2W{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter Number : ");	

		int num = sc.nextInt();
		
		int temp = num;

		//int arr[] = new int[Size];
	
		int digit = 0;

		while(num>0){

			num/=10;
			digit++;
		}

		int arr[] = new int[digit];

		for(int i = 0; i < digit && temp>0 ; i++){

			int rem = temp%10;

			arr[i] = rem + 1;

			temp/=10;

		//	System.out.println(arr[i]);

		}

		for(int i = 0; i<arr.length ; i++){

			int temp1 = arr[i];
			arr[i] = arr[arr.length - i - 1];
			arr[arr.length - i - 1] = arr[i];
			
			System.out.println(arr[i]);
		}		
		
	}

}
