import java.util.*;

class prog7_C2W{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Array size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

	
		System.out.println("Enter array elements : ");
	
		for(int i=0;i<size;i++){
			arr[i] = sc.nextInt();
		}

		System.out.print("Composite numbers in array are :");

		for(int i=0;i<size;i++){
		
			int cntd=0;
		
			int num=arr[i];
		
			for(int j=1;j<=num;j++){

				if(num%j==0){
					cntd++;
				}
			}
			if(cntd>2){
				System.out.print(num+" ");
			}
		}

		System.out.println();

	}
}


