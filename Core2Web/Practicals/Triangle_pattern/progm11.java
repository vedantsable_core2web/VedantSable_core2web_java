import java.io.*;

class progm11_C2W{

        public static void main(String[] args) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter the no. of rows : ");

                int row = Integer.parseInt(br.readLine());

                for(int i = 1; i<=row ; i++){

			int ch = i + 64;

                        for(int j = 1; j<=row-i+1 ; j++){

				if(ch%2==1){

                                System.out.print(ch + " ");

				}else{

					System.out.print((char)ch + " ");

				}
				ch++;
                        }

                        System.out.println();

                }

        }

}
