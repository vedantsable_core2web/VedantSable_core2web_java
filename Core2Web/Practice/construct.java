class demo{

	static int x = 0;

	demo(int x){

		System.out.println("In Constructor");
		this.x = x;
	}
	
	//System.out.println(x);

	void fun(int x){
		
		this.x = x;
		System.out.println(x);

	}

	public static void main(String[] args){

		demo obj = new demo(50);
		obj.fun(100);
		//System.out.println(x);
	}

}
