class bsearch{

	static int findroot(int x){
		
		int start = 1;
		int end = x;
		int ans = 0;

		while(start<end){

			int mid = (start+end)/2;
			int sqr = mid * mid;

			if(sqr == x)
				return mid;

			if(sqr > x)
				end = mid - 1;

			if(sqr < x){
				start = mid + 1;
				ans = mid;
			}
			
		}

		return ans;
	}

	public static void main(String[] args){

		int num = 100;

		int val = findroot(num);

		System.out.println(val);

	}

}
