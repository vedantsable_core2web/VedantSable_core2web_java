class Outer{
	
	int x = 10;
	static int y = 20;

	Outer(){
		System.out.println("Outer Constructor");
	}

	class Inner {

		int x = 20;

		Inner(int x){
			System.out.println("Inner Constructor");
			System.out.println(this.x);
			System.out.println(x);
			System.out.println(Outer.this.x);
		}

	}

	public static void main(String[] args){

		Outer outObj1 = new Outer();

		Inner obj = outObj1.new Inner(30);

	}

}

